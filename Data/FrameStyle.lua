-- Load Modules
local Baik = _G.Baik
local Class = Baik.Base.Class

local BaikUnitFrame = _G.BaikUnitFrame

-- Create Module
local FrameStyle = Class()

-- Public API
FrameStyle.LARGE = 1
FrameStyle.COMPACT = 2
FrameStyle.BLIZZARD = 3

-- Export Module
BaikUnitFrame.Data.FrameStyle = FrameStyle
