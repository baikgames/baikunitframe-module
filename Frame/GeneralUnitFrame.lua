-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log
local SharedMediaAction = Baik.Action.SharedMediaAction
local Table = Baik.Base.Table

local BaikUnitFrame = _G.BaikUnitFrame
local FrameAction = BaikUnitFrame.Action.FrameAction
local StatusBarAction = BaikUnitFrame.Action.StatusBarAction

-- Create Module
local GeneralUnitFrame = BaikUnitFrame:NewModule("GeneralUnitFrame")

-- Constants
local _FRAMES_PREFIX = {
    "PlayerFrame",
    "TargetFrame",
    "FocusFrame",
    "PartyMemberFrame1",
    "PartyMemberFrame2",
    "PartyMemberFrame3",
    "PartyMemberFrame4",
    "Boss1TargetFrame",
    "Boss2TargetFrame",
    "Boss3TargetFrame",
    "Boss4TargetFrame",
    "Boss5TargetFrame",
}

local _STATUS_BAR_SUFFIX = {
    "HealthBar",
    "ManaBar",
    "AlternateManaBar",
    "SpellBar",
}

-- Private Methods
local function _SetStatusBarColor(statusbar, ...)
    StatusBarAction:SetClassColor(statusbar, statusbar.unit)
end

local function _CreateStatusBars()
    local status_bars = {}
    Table.ForEach(_FRAMES_PREFIX, function(prefix)
        Table.ForEach(_STATUS_BAR_SUFFIX, function(suffix)
            local name = string.format("%s%s", prefix, suffix)
            local status_bar = _G[name]
            if status_bar ~= nil then
                table.insert(status_bars, status_bar)
            end
        end)
    end)

    return status_bars
end

-- Private API
function GeneralUnitFrame:_SetStatusBarTexture(texture_name)
    local texture = SharedMediaAction:StatusBar(texture_name)
    Table.ForEach(self._status_bars, function(status_bar)
        status_bar:SetStatusBarTexture(texture)
    end)
    CastingBarFrame:SetStatusBarTexture(texture)
end

-- Ace Callbacks
function GeneralUnitFrame:OnInitialize()
    self._status_bars = _CreateStatusBars()
    FrameAction.StatusBar:Event():Register(self, GeneralUnitFrame._SetStatusBarTexture)
    Baik:SecureHook("UnitFrameHealthBar_Update", _SetStatusBarColor)
    Baik:SecureHook("HealthBar_OnValueChanged", _SetStatusBarColor)
    Log:i("GeneralUnitFrame OnInitialize")
end

function GeneralUnitFrame:OnEnable()
    Log:i("GeneralUnitFrame OnEnable")
end

function GeneralUnitFrame:OnDisable()
    Log:i("GeneralUnitFrame OnDisable")
end
-- Export Module
BaikUnitFrame.Frame.GeneralUnitFrame = GeneralUnitFrame
