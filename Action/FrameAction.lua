-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local ProfileEvent= Baik.Event.ProfileEvent
local RepositoryAction = Baik.Action.RepositoryAction
local SharedMediaAction = Baik.Action.SharedMediaAction

local BaikUnitFrame = _G.BaikUnitFrame

-- Create Module
local FrameAction = BaikUnitFrame:NewModule("FrameAction")

-- Actions
FrameAction.Color = RepositoryAction.New("baik_unitframe_class", true)
FrameAction.StatusBar = RepositoryAction.New("baik_unitframe_status_bar",
                                             SharedMediaAction:DefaultStatusBar())

-- Private Methods
local function _TriggerEvents()
    FrameAction.Color:TriggerEvent()
    FrameAction.StatusBar:TriggerEvent()
end

-- Ace Callbacks
function FrameAction:OnInitialize()
    Log:i("FrameAction OnInitialize")
end

function FrameAction:OnEnable()
    _TriggerEvents()
    ProfileEvent:Get():Register(_TriggerEvents)
    Log:i("FrameAction OnEnable")
end

function FrameAction:OnDisable()
    Log:i("FrameAction OnDisable")
end

-- Export Module
BaikUnitFrame.Action.FrameAction = FrameAction
