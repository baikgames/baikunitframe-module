-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Table = Baik.Base.Table
local UnitAction = Baik.Action.UnitAction

local BaikUnitFrame = _G.BaikUnitFrame
local FrameAction = BaikUnitFrame.Action.FrameAction
local Layout = BaikUnitFrame.Data.Layout

-- Create Module
local StatusBarAction = BaikUnitFrame:NewModule("StatusBarAction")

-- Public API
function StatusBarAction:SetClassColor(frame, unit)
    Assert.Table(frame)

    if unit == nil then
        return
    end

    -- Don't modify color
    if frame.lockColor or frame.disconnected then
        return
    end

    local color = nil
    if UnitAction:IsPlayer(unit) and FrameAction.Color:Get() then
        color = UnitAction:ClassColor(unit)
    else
        color = UnitAction:UnitColor(unit)
    end

    frame:SetStatusBarColor(unpack(color))
end

-- Export Module
BaikUnitFrame.Action.StatusBarAction = StatusBarAction
