-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local HideFrame = Baik.Frame.HideFrame
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikUnitFrame = _G.BaikUnitFrame
local FrameAction = BaikUnitFrame.Action.FrameAction
local FrameStyle = BaikUnitFrame.Data.FrameStyle
local Layout = BaikUnitFrame.Data.Layout
local LayoutAction = BaikUnitFrame.Action.LayoutAction
local StatusBarAction = BaikUnitFrame.Action.StatusBarAction
local TextureAction = BaikUnitFrame.Action.TextureAction

-- Create Module
local TargetUnitFrame = BaikUnitFrame:NewModule("TargetUnitFrame")

-- Constants
local _UNIT_FRAME = {
    "TargetFrame",
    "FocusFrame",
    "Boss1TargetFrame",
    "Boss2TargetFrame",
    "Boss3TargetFrame",
    "Boss4TargetFrame",
    "Boss5TargetFrame"
}

local _LARGE_LAYOUTS = {
    ["HealthBar"] = Layout:New({{"TOPRIGHT", -106, -22}}, 119, 31),
    ["TextureFrameHealthBarText"] = Layout:New({{"CENTER", -50, 7}}),
    ["TextureFrameHealthBarTextLeft"] = Layout:New({{"LEFT", 8, 7}}),
    ["TextureFrameHealthBarTextRight"] = Layout:New({{"RIGHT", -110, 7}}),
}

-- Private Methods
local function _SetupLargeFrame()
    Table.ForEach(_UNIT_FRAME, function(frame_prefix)
        local texture_name = string.format("%sTextureFrameTexture", frame_prefix)
        local texture = _G[texture_name]
        Assert.NotNull(texture)
        TextureAction:SetLargeTexture(texture)

        Table.ForEach(_LARGE_LAYOUTS, function(layout, frame_suffix)
            local frame_name = string.format("%s%s", frame_prefix, frame_suffix)
            local frame = _G[frame_name]
            Assert.NotNull(frame)
            LayoutAction:SetLayout(frame, layout)
        end)
    end)
end

local function _SetupClassColor()
    Table.ForEach(_UNIT_FRAME, function(frame_prefix)
        local status_name = string.format("%sHealthBar", frame_prefix)
        local status_bar = _G[status_name]
        Assert.NotNull(status_bar)

        StatusBarAction:SetClassColor(status_bar, status_bar.unit)
    end)
end

local function _SetupNameColor(frame)
    frame.nameBackground:SetVertexColor(0.0, 0.0, 0.0, 0.5)
end

local function _SetupTexture(frame)
    Assert.NotNull(frame)

    local texture = frame.borderTexture
    Assert.NotNull(texture)
    TextureAction:SetLargeTexture(texture)
end

-- Ace Callbacks
function TargetUnitFrame:OnInitialize()
    Log:i("TargetUnitFrame OnInitialize")
end

function TargetUnitFrame:OnEnable()
    _SetupLargeFrame()
    FrameAction.Color:Event():Register(_SetupClassColor)
    Baik:SecureHook("TargetFrame_CheckFaction", _SetupNameColor)
    Baik:SecureHook("TargetFrame_CheckClassification", _SetupTexture)
    Log:i("TargetUnitFrame OnEnable")
end

function TargetUnitFrame:OnDisable()
    Log:i("TargetUnitFrame OnDisable")
end
-- Export Module
BaikUnitFrame.Frame.TargetUnitFrame = TargetUnitFrame
