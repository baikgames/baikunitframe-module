-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Table = Baik.Base.Table

local BaikUnitFrame = _G.BaikUnitFrame
local Layout = BaikUnitFrame.Data.Layout

-- Create Module
local LayoutAction = BaikUnitFrame:NewModule("LayoutAction")

-- Public API
function LayoutAction:SetSize(frame, layout)
    Assert.Table(frame)
    Assert.Child(layout, Layout)

    local width = layout:Width()
    local height = layout:Height()
    if width == nil or height == nil then
        return
    end
    frame:SetSize(width, height)
end

function LayoutAction:SetAnchors(frame, layout)
    Assert.Table(frame)
    Assert.Child(layout, Layout)

    local anchors = layout:Anchors()
    frame:ClearAllPoints()
    Table.ForEach(anchors, function(anchor)
        frame:SetPoint(unpack(anchor))
    end)
end

function LayoutAction:SetLayout(frame, layout)
    Assert.Table(frame)
    Assert.Child(layout, Layout)

    self:SetSize(frame, layout)
    self:SetAnchors(frame, layout)
end

-- Export Module
BaikUnitFrame.Action.LayoutAction = LayoutAction
