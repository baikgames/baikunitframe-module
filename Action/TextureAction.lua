-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class
local Log = Baik.Base.Log
local ProfileEvent= Baik.Event.ProfileEvent
local RepositoryAction = Baik.Action.RepositoryAction

local BaikUnitFrame = _G.BaikUnitFrame

-- Create Module
local TextureAction = BaikUnitFrame:NewModule("TextureAction")

-- Constants
local _TEXTURES = {
    ["Interface\\CharacterFrame\\UI-Player-Status"] = "Interface\\Addons\\BaikUnitFrame\\Texture\\UI-Player-Status",
    ["Interface\\TargetingFrame\\UI-TargetingFrame"] = "Interface\\TargetingFrame\\TargetFrame",
    ["Interface\\TargetingFrame\\UI-TargetingFrame-Elite"] = "Interface\\TargetingFrame\\TargetFrameElite",
    ["Interface\\TargetingFrame\\UI-TargetingFrame-Rare"] = "Interface\\TargetingFrame\\TargetFrameRare",
    ["Interface\\TargetingFrame\\UI-TargetingFrame-RareElite"] = "Interface\\TargetingFrame\\TargetFrameRareElite",
    ["Interface\\TargetingFrame\\UI-UnitFrame-Boss"] = "Interface\\TargetingFrame\\UnitFrameBoss"
}

-- Public API
function TextureAction:SetLargeTexture(texture)
    Assert.Table(texture)
    Assert.Function(texture.GetTexture)

    local cur_texture = texture:GetTexture()
    local new_texture = _TEXTURES[cur_texture]
    if new_texture == nil then
        return
    end

    texture:SetTexture(new_texture)
end

-- Ace Callbacks
function TextureAction:OnInitialize()
    Log:i("TextureAction OnInitialize")
end

function TextureAction:OnEnable()
    Log:i("TextureAction OnEnable")
end

function TextureAction:OnDisable()
    Log:i("TextureAction OnDisable")
end

-- Export Module
BaikUnitFrame.Action.TextureAction = TextureAction
