-- Load Module
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Log = Baik.Base.Log
local Table = Baik.Base.Table

local BaikUnitFrame = _G.BaikUnitFrame
local FrameAction = BaikUnitFrame.Action.FrameAction
local FrameStyle = BaikUnitFrame.Data.FrameStyle
local Layout = BaikUnitFrame.Data.Layout
local LayoutAction = BaikUnitFrame.Action.LayoutAction
local StatusBarAction = BaikUnitFrame.Action.StatusBarAction
local TextureAction = BaikUnitFrame.Action.TextureAction

-- Create Module
local PlayerUnitFrame = BaikUnitFrame:NewModule("PlayerUnitFrame")

-- Constants
local _LAYOUTS = {
    [PlayerFrameHealthBar] = Layout:New({{"TOPLEFT", 106, -22}}, 119, 31),
    [PlayerFrameHealthBarText] = Layout:New({{"CENTER", 50, 7}}),
    [PlayerFrameHealthBarTextLeft] = Layout:New({{"LEFT", 110, 7}}),
    [PlayerFrameHealthBarTextRight] = Layout:New({{"RIGHT", -8, 7}}),
    [PlayerStatusTexture] = Layout:New({{"TOPLEFT", 36, -9}})
}

-- Private Methods
local function _SetupLargeFrame()
    TextureAction:SetLargeTexture(PlayerFrameTexture)
    TextureAction:SetLargeTexture(PlayerStatusTexture)

    Table.ForEach(_LAYOUTS, function(layout, frame)
        LayoutAction:SetLayout(frame, layout)
    end)
end

local function _SetupClassColor()
    StatusBarAction:SetClassColor(PlayerFrameHealthBar, PlayerFrameHealthBar.unit)
end

-- Ace Callbacks
function PlayerUnitFrame:OnInitialize()
    Log:i("PlayerUnitFrame OnInitialize")
end

function PlayerUnitFrame:OnEnable()
    _SetupLargeFrame()
    FrameAction.Color:Event():Register(_SetupClassColor)
    Baik:SecureHook("PlayerFrame_ToPlayerArt", _SetupLargeFrame)
    Baik:SecureHook("PlayerFrame_ToVehicleArt", _SetupLargeFrame)
    Log:i("PlayerUnitFrame OnEnable")
end

function PlayerUnitFrame:OnDisable()
    Log:i("PlayerUnitFrame OnDisable")
end
-- Export Module
BaikUnitFrame.Frame.PlayerUnitFrame = PlayerUnitFrame
