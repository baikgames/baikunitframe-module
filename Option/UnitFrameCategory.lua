-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local ToggleArg = Baik.Option.Arg.ToggleArg

local BaikUnitFrame = _G.BaikUnitFrame
UnitFrameAction = BaikUnitFrame.Action.UnitFrameAction

-- Create Module
local UnitFrameCategory = Category.New("UnitFrame", "unit_frame", Category.TAB, 1)

-- Setup Module
Option:Category():Add(UnitFrameCategory)

-- Export Module
Baik.Option.UnitFrameCategory = UnitFrameCategory
