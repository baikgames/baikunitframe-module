-- Load Modules
local Baik = _G.Baik
local Assert = Baik.Base.Assert
local Class = Baik.Base.Class

local BaikUnitFrame = _G.BaikUnitFrame

-- Create Module
local Layout = Class()

-- Public API
function Layout:New(anchors, width, height)
    Assert.Table(anchors)

    local obj = Class(self)
    obj._anchors = anchors
    obj._width = width
    obj._height = height

    return obj
end

function Layout:Width()
    return self._width
end

function Layout:Height()
    return self._height
end

function Layout:Anchors()
    return self._anchors
end

-- Export Module
BaikUnitFrame.Data.Layout = Layout
