-- Load Module
local _G = _G
local Baik = _G.Baik
local Log = Baik.Base.Log

-- Create Module
local BaikUnitFrame = Baik:NewModule("BaikUnitFrame")

-- Ace Callbacks
function BaikUnitFrame:OnInitialize()
    Log:i("BaikUnitFrame OnInitialize")
end

function BaikUnitFrame:OnEnable()
    Log:i("BaikUnitFrame OnEnable")
end

function BaikUnitFrame:OnDisable()
    Log:i("BaikUnitFrame OnDisable")
end

-- Export Module
_G.BaikUnitFrame = BaikUnitFrame
