-- Load Module
local Baik = _G.Baik
local Category = Baik.Option.Category
local Option = Baik.Option.Option
local StatusBarArg = Baik.Option.Arg.StatusBarArg
local ToggleArg = Baik.Option.Arg.ToggleArg
local VisibilityArg = Baik.Option.Arg.VisibilityArg

local BaikUnitFrame = _G.BaikUnitFrame
local FrameAction = BaikUnitFrame.Action.FrameAction
local UnitFrameCategory = Baik.Option.UnitFrameCategory

-- Create Module
local GeneralCategory = Category.New("General", "general", Category.TREE, 0)

-- Constants
local _ARGS = {
    ToggleArg.New("Use Class Color", "class_color", FrameAction.Color, 0, ToggleArg.WIDTH_FULL),
    StatusBarArg.New("StatusBar Texture", "status_texture", FrameAction.StatusBar, 1, ToggleArg.WIDTH_FULL)
}

-- Setup Module
GeneralCategory:AddAll(_ARGS)
UnitFrameCategory:Add(GeneralCategory)

-- Export Module
Baik.Option.GeneralCategory = GeneralCategory
